<?php

/**
 * Class News
 * @ @property $id
 * @ @property $title
 * @ @property $text
 */
class News extends AbstractModel{

    protected static $table = 'news';

}