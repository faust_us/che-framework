<?php

class DB{

    private $dbh;
    private $className = 'stdClass';

    public function __construct(){

        $this->dbh = new PDO( 'mysql:dbname=che;host=localhost', 'root', '' );

    }

    public function setClassName( $className ){

        $this->className = $className;

    }

    public function query( $sql, $params=[] ){

        $sth = $this->dbh->prepare( $sql );
        $sth->execute( $params );
        return $sth->fetchAll(PDO::FETCH_CLASS, $this->className);

    }

    public function execute( $sql, $params=[] ){

        $sth = $this->dbh->prepare( $sql );
        $sth->execute( $params );

    }

    public static function prepareParams ( $params ){

        $data = [];
        foreach( $params as $key => $v ){
            $data[ ':'.$key ] = $v;
        }

        return $data;
    }

    public function lastInsertId(){

        return $this->dbh->lastInsertId();

    }

}
