<?php

class View implements Iterator{

    protected $data = [];

    public function __set( $k, $v){

        $this->data[ $k ] = $v;

    }

    public function __get( $k ){

        return $this->data[ $k ];

    }

    public function display( $template ){

        echo $this->render( $template );

    }

    public function render( $template ){

        foreach ( $this->data as $key => $value ){
            $$key = $value;
        }

        ob_start();
        include __DIR__ . '/../views/' . $template . '.php';
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    public function current()
    {
        return current( $this->data );
    }

    public function next()
    {
        return next( $this->data );
    }

    public function key()
    {
        return key( $this->data );
    }

    public function valid()
    {
        return  $this->key() !== null &&  $this->key() !== false;
    }

    public function rewind()
    {
        reset( $this->data );
    }

    public function count(){
        return count( $this->data );
    }

}