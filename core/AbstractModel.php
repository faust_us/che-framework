<?php

abstract class AbstractModel{

    protected static $table;
    protected static $class;

    protected $data;

    public function __construct(){

        static::$class = get_called_class();

    }

    public function __get( $k ){

        return $this->data[ $k ];

    }

    public function __set( $k, $v ){

        $this->data[ $k ] = $v;

    }

    public function __isset( $k ){

        return isset( $this->data[ $k ] );

    }

    public static function findAll(){

        $sql = 'select * from ' . static::$table;
        $db = new DB();
        $db->setClassName( get_called_class() );
        return $db->query( $sql );

    }

    public static function findOneByPK( $id ){

        $sql = 'select * from ' . static::$table . ' where id=:value';
        $db = new DB();
        $db->setClassName( get_called_class() );
        return $db->query( $sql, [ ':value' => $id ] )[0];

    }

    public static function findOneByColumn( $column, $value ){

        $sql = 'select * from ' . static::$table . ' where ' . $column . '=:value';
        $db = new DB();
        $db->setClassName( get_called_class() );
        $res = $db->query( $sql, [ ':value' => $value ] );
        return !empty( $res ) ? $res[0] : false;

    }

    public function insert(){

        $prepared_data = DB::prepareParams( $this->data );

        $sql = '
          INSERT INTO ' . static::$table . '
          (' . implode( ',', array_keys( $this->data ) ) . ')
          VALUES
          (' . implode( ',', array_keys( $prepared_data ) ) . ')
        ';

        $db = new DB();
        $db->query( $sql, $prepared_data );
        $this->id = $db->lastInsertId();
    }

    public function update(){

        $cols = [];
        $values = [];
        foreach( $this->data as $key => $v ){
            $values[ ':' . $key ] = $v;
            if ( $key === 'id' )
                continue;
            $cols[] = $key . '=:' . $key;

        }
        $sql = '
            UPDATE ' . static::$table . '
            SET '. implode( ',', $cols ) .'
            WHERE id=:id
        ';

        $db = new DB();
        $db->execute( $sql, $values );
    }

    public function save(){
        if ( isset( $this->id ) )
            $this->update();
        else
            $this->insert();
    }

}