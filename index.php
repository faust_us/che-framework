<?php

require_once __DIR__ . '/core/autoload.php';

$ctrl = $_GET['ctrl'] ? $_GET['ctrl'] : 'News';
$act = $_GET['act'] ? $_GET['act'] : 'All';

$controllerClassName = $ctrl . 'Controller';
$controller = new $controllerClassName();
$method = 'action' . $act;
$controller->$method();