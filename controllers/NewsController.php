<?php

class NewsController{

    public function actionAll(){

        $article = new News();
        $article->id = 2;
        $article->text = 'Изменил';
        $article->update();

    }

    public function actionOne( ){
        $item = News::findOneByColumn( 'title', 'Первая новость' );
        if ( false !== $item ){
            $view = new View();
            $view->item = $item;
            $view->display('news/one');
        }
    }

}